# Demo Project

This demo project serves as a demonstration for applying the Cucumber Framework found in the [framework repository](https://gitlab.com/the-ta-crowd/cucumber/cucumber-framework).

## Properties
Properties are stored in the profiles of the application using the Springboot way.
In Maven custom profiles are created to make selection easier.

When running from JUnit or Cucumber it is needed to set the active profile
```
    -Dspring.profiles.active=dev
```
