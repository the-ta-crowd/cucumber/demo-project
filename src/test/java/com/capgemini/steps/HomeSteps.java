package com.capgemini.steps;

import com.capgemini.steps.common.BaseSteps;
import io.cucumber.java.en.When;

public class HomeSteps extends BaseSteps {

    @When("the user searches for {string} in {string}")
    public void theUserSearchesForIn(final String jobTitle, final String location) {
        pages.home.searchFor(jobTitle, location);
    }

}
