package com.capgemini.steps.common;

import io.cucumber.java.en.Given;

public class CommonSteps extends BaseSteps {

    @Given("the user is on the homepage")
    public void theUserOpensTheHomepage() {
        pages.common.navigateTo("");
    }

    @Given("the user is on {string}")
    public void theUserIsOn(String uri) {
        pages.common.navigateTo(uri);
    }

}
