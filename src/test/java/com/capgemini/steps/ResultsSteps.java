package com.capgemini.steps;

import com.capgemini.steps.common.BaseSteps;
import io.cucumber.java.en.Then;

public class ResultsSteps extends BaseSteps {

    @Then("the user sees search results")
    public void theUserSeesSearchResults() {
        pages.results.verifySearchResultsPresent();
    }
}
