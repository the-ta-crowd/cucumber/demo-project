package com.capgemini.pages.common;

import com.capgemini.webdriver.Webdriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BasePage {

    @Autowired
    protected Webdriver driver;

}
