package com.capgemini.pages.common;

import com.capgemini.pages.HomePage;
import com.capgemini.pages.ResultsPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Pages {

    @Autowired
    public CommonPage common;
    @Autowired
    public HomePage home;
    @Autowired
    public ResultsPage results;
}
