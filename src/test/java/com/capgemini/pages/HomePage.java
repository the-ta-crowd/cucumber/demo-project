package com.capgemini.pages;

import com.capgemini.pages.common.BasePage;
import org.openqa.selenium.By;
import org.springframework.stereotype.Component;

@Component
public class HomePage extends BasePage {

    private static final String JOB_TITLE_FIELD_SELECTOR = "input#text-input-what";
    private static final String JOB_LOCATION_FIELD_SELECTOR = "input#text-input-where";
    private static final String SUBMIT_BUTTON_SELECTOR = "button[type='submit']";

    public void searchFor(final String jobTitle, final String location) {
        driver.findElement(By.cssSelector(JOB_TITLE_FIELD_SELECTOR)).sendKeys(jobTitle);
        driver.findElement(By.cssSelector(JOB_LOCATION_FIELD_SELECTOR)).sendKeys(location);
        driver.findElement(By.cssSelector(SUBMIT_BUTTON_SELECTOR)).click();
    }

}
