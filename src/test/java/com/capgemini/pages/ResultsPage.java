package com.capgemini.pages;

import com.capgemini.pages.common.BasePage;
import org.openqa.selenium.By;
import org.springframework.stereotype.Component;

import static org.junit.Assert.assertNotNull;

@Component
public class ResultsPage extends BasePage {

    private static final String SEARCH_RESULTS_BLOCK_SELECTOR = "tbody#resultsBodyContent";

    public void verifySearchResultsPresent() {
        assertNotNull(driver.findElement(By.cssSelector(SEARCH_RESULTS_BLOCK_SELECTOR)));
    }
}
