Feature: Search for stuff

  Scenario: As a user, I can search for job openings from the homepage
    Given the user is on the homepage
    When the user searches for "Test Automation Engineer" in "Amersfoort"
    Then the user sees search results