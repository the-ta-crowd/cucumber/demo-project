package com.capgemini;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/java/com/capgemini/features"},
        glue = {"com.capgemini.steps", "com.capgemini.hooks"},
        plugin = {"pretty", "html:target/cucumber-html", "json:target/report.json"},
        strict = true)
public class TestRunner {

}
